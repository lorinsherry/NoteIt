//
//  constants.swift
//  NoteIt
//
//  Created by Lorin Sherry on 10/04/2022.
//

struct K {
    static let appName = "NoteIt✍🏻"
    static let registerSegue = "RegisterToNotes"
    static let loginSegue = "LoginToNotes"
}
