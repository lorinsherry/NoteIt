//
//  WelcomeViewController.swift
//  NoteIt
//
//  Created by Lorin Sherry on 10/04/2022.
//

import UIKit
import CLTypingLabel

class WelcomeViewController: UIViewController {

    
    @IBOutlet weak var titleLabel: CLTypingLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = K.appName
    }
    


}
